package file_upload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet("/FileUploaderServlet")
public class FileUploaderServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		Part p = request.getPart("file");
		String fname = p.getSubmittedFileName();
		String path = "./src/main/webapp/uploads/";
		File f = new File(path, fname);
		String cb[] = request.getParameterValues("cb");
		if(cb!=null)
		{
			//checkbox checked
			if(f.exists())
			{
				f.delete();
				for(Part s : request.getParts())
				{
					s.write(path+fname);
				}
				out.println("File is Overwritten");
			}
			else
			{
				for(Part s : request.getParts())
				{
					s.write(path+fname);
				}
				out.println("File Written");
			}
		}
		else
		{
			//checkbox unchecked
			if(f.exists())
			{
				out.println("File already exists and OverWrite not checked");
			}
			else
			{
				for(Part s : request.getParts())
				{
					s.write(path+fname);
				}
				out.println("File Written");
			}
		}
	}

}
