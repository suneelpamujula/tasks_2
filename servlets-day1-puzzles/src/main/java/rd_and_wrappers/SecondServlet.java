package rd_and_wrappers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SecondServlet")
public class SecondServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		
		out.println("This is second servlet");
		out.println(" Welcome "+firstName+" "+lastName);
		Test t = new Test();
		out.println(t.show());
	}

}
