package rd_and_wrappers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/FirstServlet")
public class FirstServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		
		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		
		if(firstName!=null&&lastName!=null)
		{
			RequestDispatcher rd = request.getRequestDispatcher("SecondServlet");
			rd.forward(request, response);
		}
		else
		{
			out.println("Name is null");
			RequestDispatcher rd = request.getRequestDispatcher("rd.html");
			rd.include(request, response);
		}
	}

}
