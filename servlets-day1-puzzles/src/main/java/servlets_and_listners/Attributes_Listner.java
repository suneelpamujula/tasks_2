package servlets_and_listners;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Attributes_Listner implements ServletRequestAttributeListener {
	
	public void attributeAdded(ServletRequestAttributeEvent servletRequestAttributeEvent)  { 
        System.out.println("************************************");
        System.out.println("addAttribute method called");
        System.out.println("Newly added attribute Name: "+servletRequestAttributeEvent.getName()+" -------- Value: "+servletRequestAttributeEvent.getValue());
        System.out.println("************************************");
    }
    
    public void attributeReplaced(ServletRequestAttributeEvent servletRequestAttributeEvent)  { 
    	System.out.println("************************************");
        System.out.println("replaceAttribute method called");
        System.out.println("Replaced attribute Name: "+servletRequestAttributeEvent.getName()+" -------- Value: "+servletRequestAttributeEvent.getValue());
        System.out.println("************************************");
    }
    
    public void attributeRemoved(ServletRequestAttributeEvent servletRequestAttributeEvent)  { 
    	System.out.println("************************************");
        System.out.println("removeAttribute method called");
        System.out.println("Removed attribute Name: "+servletRequestAttributeEvent.getName()+" -------- Value: "+servletRequestAttributeEvent.getValue());
        System.out.println("************************************");
    }
}

