package servlets_and_listners;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/attributeslistner")
public class Attributes_Listner_Servlet extends HttpServlet{

	public void service(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		request.setAttribute("language", "java");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		request.setAttribute("language", "python");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		request.removeAttribute("language");
	}
}
