package forms_and_parameters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/AuthServlet")
public class AuthServlet extends HttpServlet {
	
	Map<String, String> map;
    @Override
    public void init() throws ServletException {
    	map = new HashMap();
    	map.put("name1", "pass1");
    	map.put("name2", "pass2");
    	map.put("name3", "pass3");
    	map.put("name4", "pass4");
    	map.put("name5", "pass5");
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		
		for(Map.Entry<String, String> data:map.entrySet())
		{
			if(name.equals(data.getKey())&&password.equals(data.getValue()))
			{
				out.println("Login successfull"+"</br>");
				out.println("Name/Password Match");
			}
			else
			{
				out.println("Name/Password Does Not Match");
				RequestDispatcher rd = request.getRequestDispatcher("LoginForm.html");
				rd.include(request, response);
			}
		}		
	}

}
